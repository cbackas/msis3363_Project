﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace dummyforums {
    public partial class Stats : System.Web.UI.Page {
        string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\dummyforum.mdf;Integrated Security=True;Connect Timeout=30";

        //Page loads - this activates ddlStatType_Changed to make sure that on first load the correct panels are visible
        protected void Page_Load(object sender, EventArgs e) {
            ddlStatType_Changed(sender, e);
        }

        //changes what panel of information is visible depending on what DDL item is selected
        protected void ddlStatType_Changed(object sender, EventArgs e) {
            bool postsSelected = ddlStatType.SelectedValue == "posts";
            bool usersSelected = ddlStatType.SelectedValue == "users";

            panelPosts.Visible = postsSelected;
            panelUsers.Visible = usersSelected;

            if (postsSelected) {
                updatePostsCounts();
            } else if (usersSelected) {
                updateUserCounts();
            }

        }

        //runs the SQL necesarry to set the lbls for statistics in the Posts panel
        protected void updatePostsCounts() {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmdPosts = new SqlCommand("SELECT COUNT(*) FROM [Threads]", con);
            SqlCommand cmdComments = new SqlCommand("SELECT COUNT(*) FROM [Comments]", con);

            int postCount = (int)cmdPosts.ExecuteScalar();
            int commentCount = (int)cmdComments.ExecuteScalar();
            con.Close();

            lblPostCount.Text = Convert.ToString(postCount);
            lblCommentCount.Text = Convert.ToString(commentCount);
        }

        //runs the SQL necesarry to set the lbls for statistics in the Users panel
        protected void updateUserCounts() {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmdUniqueUsersCount = new SqlCommand("SELECT COUNT(poster) AS Expr1 FROM (SELECT poster FROM Threads AS t GROUP BY poster UNION SELECT poster FROM Comments AS c GROUP BY poster) AS thing", con);
            
            int userCount = (int)cmdUniqueUsersCount.ExecuteScalar();
            con.Close();

            lblNickCount.Text = Convert.ToString(userCount);
        }

        //gridview pre-render
        protected void grdThreads_PreRender(object sender, EventArgs e) {
            grdThreads.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        //gridview pre-render
        protected void grdTopUsers_PreRender(object sender, EventArgs e) {
            grdTopUsers.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        //when you select a post in the Post panel this takes you to said post
        protected void grdThreads_SelectedIndexChanged(object sender, EventArgs e) {
            GridViewRow row = grdThreads.SelectedRow;
            String url = "Post.aspx" + "?id=" + row.Cells[0].Text;
            Response.Redirect(url);
        }


    }
}