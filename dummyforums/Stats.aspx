﻿<%@ Page Title="Stats" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Stats.aspx.cs" Inherits="dummyforums.Stats" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <nav>
        <ul class="nav nav-tabs">
            <li><a href="Default.aspx">Home</a></li>
            <li><a href="NewPost.aspx">New Post</a></li>
            <li class="active"><a href="Stats.aspx">Stats</a></li>
            <li><a href="Search.aspx">Search</a></li>
        </ul>
    </nav>
    <br />
    <br />

    <div class="row">
        <div class="col-xs-3">
            <asp:DropDownList ID="ddlStatType" runat="server" OnSelectedIndexChanged="ddlStatType_Changed" AutoPostBack="true">
                <asp:ListItem Value="posts" Text="Posts"></asp:ListItem>
                <asp:ListItem Value="users" Text="Users"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>

    <asp:Panel ID="panelPosts" runat="server" Visible="true">
        <div class="row padded">
            <div class="col-xs-2"></div>
            <div class="col-xs-4 stats">
                Total Posts:
                <p>
                    <asp:Label ID="lblPostCount" runat="server" Text="0" CssClass="bignumber"></asp:Label>
                </p>
            </div>
            <div class="col-xs-4 stats">
                Total Comments:
                <p>
                    <asp:Label ID="lblCommentCount" runat="server" Text="0" CssClass="bignumber"></asp:Label>
                </p>
            </div>
        </div>
        <div class="row padded text-center">
            <h4>Top 5 posts with the most comments:</h4>
        </div>
        <div class="row padded">
            <asp:GridView ID="grdThreads" runat="server"
                AutoGenerateColumns="False"
                CssClass="table table-bordered table-striped table-condensed"
                DataKeyNames="Id" DataSourceID="ThreadsSQL"
                OnPreRender="grdThreads_PreRender"
                OnSelectedIndexChanged="grdThreads_SelectedIndexChanged">
                <Columns>
                    <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" HeaderStyle-CssClass="hidden" SortExpression="Id">
                        <ItemStyle CssClass="hidden" />
                    </asp:BoundField>
                    <asp:BoundField DataField="count" HeaderText="Count" SortExpression="Count">
                        <ItemStyle CssClass="col-xs-1" />
                    </asp:BoundField>
                    <asp:BoundField DataField="poster" HeaderText="User" SortExpression="poster">
                        <ItemStyle CssClass="col-xs-1" />
                    </asp:BoundField>
                    <asp:ButtonField DataTextField="Title" HeaderText="Title" SortExpression="title" CommandName="Select">
                        <ItemStyle CssClass="col-xs-8" />
                    </asp:ButtonField>
                    <asp:BoundField DataField="datetime" DataFormatString="{0:d}" HeaderText="Date" SortExpression="datetime">
                        <ItemStyle CssClass="col-xs-2" />
                    </asp:BoundField>
                </Columns>
                <HeaderStyle CssClass="card-header" />

            </asp:GridView>
            <asp:SqlDataSource ID="ThreadsSQL" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT TOP 5 Threads.Id, Threads.poster, Threads.title, Threads.datetime, COUNT(Comments.threadID) AS count FROM Threads INNER JOIN Comments ON Comments.threadID = Threads.Id GROUP BY Threads.Id, Threads.poster, Threads.title, Threads.datetime ORDER BY count DESC, datetime DESC" OldValuesParameterFormatString="original_{0}"></asp:SqlDataSource>
        </div>
    </asp:Panel>
    
    <asp:Panel ID="panelUsers" runat="server" Visible="false">
        <div class="row padded">
            <div class="col-xs-4"></div>
            <div class="col-xs-4 stats">
                Unique Nicknames:
                <p>
                    <asp:Label ID="lblNickCount" runat="server" Text="0" CssClass="bignumber"></asp:Label>
                </p>
            </div>
        </div>

        <div class="row padded">
            <h4>Most active users:</h4>
        </div>

        <div class="col-xs-4">
            <asp:GridView ID="grdTopUsers" runat="server"
                AutoGenerateColumns="False"
                CssClass="table table-bordered table-striped table-condensed"
                DataKeyNames="poster" DataSourceID="TopUsersSQL"
                OnPreRender="grdTopUsers_PreRender">
                <Columns>
                    <asp:BoundField DataField="poster" HeaderText="User" SortExpression="poster">
                        <ItemStyle CssClass="col-xs-2" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Expr1" HeaderText="Count" SortExpression="Count">
                        <ItemStyle CssClass="col-xs-2" />
                    </asp:BoundField>
                </Columns>
                <HeaderStyle CssClass="card-header" />
            </asp:GridView>
            </div>
            <asp:SqlDataSource ID="TopUsersSQL" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT TOP (5) poster, SUM(count) AS Expr1 FROM (SELECT poster, COUNT(Id) AS count FROM Threads AS t GROUP BY poster UNION SELECT poster, COUNT(Id) AS count FROM Comments AS c GROUP BY poster) AS thing GROUP BY poster ORDER BY Expr1 DESC" OldValuesParameterFormatString="original_{0}"></asp:SqlDataSource>

    </asp:Panel>
</asp:Content>
