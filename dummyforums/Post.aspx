﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Post.aspx.cs" Inherits="dummyforums.Post" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <nav>
        <ul class="nav nav-tabs">
            <li><a href="Default.aspx">Home</a></li>
            <li><a href="NewPost.aspx">New Post</a></li>
            <li><a href="Stats.aspx">Stats</a></li>
            <li><a href="Search.aspx">Search</a></li>
        </ul>
    </nav>
    <br />
    <br />

    <asp:Label ID="lblPostID" runat="server" Text="" Visible="False"></asp:Label>

    <asp:DetailsView ID="DetailsView1" runat="server"
        DataSourceID="SqlDataSource1" AutoGenerateRows="False"
        DataKeyNames="Id"
        CssClass="table table-bordered table-striped table-condensed">
        <Fields>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" Visible="False" SortExpression="Id" />

            <asp:BoundField DataField="title" HeaderText="Title:" SortExpression="title">
                <ItemStyle CssClass="col-xs-12" />
            </asp:BoundField>
            <asp:BoundField DataField="poster" HeaderText="Author:" SortExpression="poster">
                <ItemStyle CssClass="col-xs-6" />
            </asp:BoundField>
            <asp:BoundField DataField="datetime" HeaderText="Date:" SortExpression="datetime">
                <ItemStyle CssClass="col-xs-6" />
            </asp:BoundField>
            <asp:BoundField DataField="content" HeaderText="Content:" SortExpression="content">
                <ItemStyle CssClass="col-xs-12" />
            </asp:BoundField>

        </Fields>
    </asp:DetailsView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Threads] WHERE ([Id] = @Id)">
        <SelectParameters>
            <asp:ControlParameter ControlID="lblPostID" Name="Id" PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

    <div class="padded">
        <h3>Comments</h3>
        <asp:DataList ID="DataList1" runat="server" DataKeyField="Id" DataSourceID="SqlDataSource2">
            <ItemTemplate>
                <table class="table-bordered table-condensed table">
                    <div class="row">
                        <div class="col-xs-12">
                            Author:
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("poster") %>' />
                            &nbsp;|&nbsp;
                        <asp:Label ID="Label2" runat="server" Text='<%# Eval("datetime") %>' />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("content") %>' />
                        </div>
                    </div>
                    <p />
                </table>

            </ItemTemplate>
        </asp:DataList>
    </div>

    <br />

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" HeaderText="* means that the field is required" />

    <h2>New Comment:</h2>
    <div class="row padded">
        <div class="col-xs-3">
            <asp:TextBox ID="txtNickName" runat="server"
                Placeholder="Nickname"
                CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-xs-4">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="txtNickName" CssClass="text-danger"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                runat="server" Display="dynamic"
                ControlToValidate="txtNickName"
                ValidationExpression="^([\S\s]{0,30})$"
                ErrorMessage="Nickname maximum: 30 characters"
                CssClass="text-danger">
            </asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="row padded">
        <div class="col-xs-8">
            <asp:TextBox ID="txtContent" runat="server"
                TextMode="MultiLine" Rows="6"
                Placeholder="New Comment..."
                CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-xs-4">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="txtContent" CssClass="text-danger"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                runat="server" Display="dynamic"
                ControlToValidate="txtContent"
                ValidationExpression="^([\S\s]{0,1000})$"
                ErrorMessage="Comment maximum: 1000 characters"
                CssClass="text-danger">
            </asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-1">
            <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn" />
        </div>
    </div>

    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Comments] WHERE ([threadID] = @threadID) ORDER BY [datetime]" InsertCommand="INSERT INTO Comments(Id, poster, [content], datetime, threadID) VALUES (,,,,)">
        <SelectParameters>
            <asp:ControlParameter ControlID="lblPostID" Name="threadID" PropertyName="Text" Type="Int32" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>
