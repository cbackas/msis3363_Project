﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace dummyforums {
    public partial class Search : System.Web.UI.Page {
        string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\dummyforum.mdf;Integrated Security=True;Connect Timeout=30";

        int searchType = 1;

        //On page load, URL is checked for a query and if a search exists it tries to look for said search in the threads table depending on what search you chose in the radiobuttons (title or content column)
        protected void Page_Load(object sender, EventArgs e) {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

            grdTitle.Visible = false;
            grdContent.Visible = false;
            radiobtnTitle.Checked = true;

            string search = Request.QueryString["search"];
            if (search != null) {
                string decodedSearch = Uri.UnescapeDataString(search);
                lblSearch.Text = decodedSearch;

                string st = Request.QueryString["type"];
                bool isNumber = System.Text.RegularExpressions.Regex.IsMatch(st, @"^\d+$");
                
                //make sure we're dealing with a real number so that we can compare it to our 2 options (1 and 2)
                if (isNumber) {
                    int num = Convert.ToInt32(st);
                    if (num == 1 || num == 2) {
                        searchType = num;
                    }
                }

                //depending on searchType, the column searched with SQL changes
                if (searchType == 1) {
                    if (foundSearchResults(1)) {
                        grdTitle.Visible = true;
                        radiobtnTitle.Checked = true;
                    } else {
                        lblmessage.Text = "Couldn't find anything matching your search: '" + search + "'";
                    }
                } else if (searchType == 2) {
                    if (foundSearchResults(2)) {
                        grdContent.Visible = true;
                        radiobtnContent.Checked = true;
                    } else {
                        lblmessage.Text = "Couldn't find anything matching your search: '" + search + "'";
                    }
                }
            }
        }

        //When you hit search this happens. It refreshes the page and appends your search to the URL.
        //I used Uri.EscapeDataString to encode the query just incase
        protected void btnSubmit_Click(object sender, EventArgs e) {
            if (IsValid) {
                string query = txtSearch.Text;
                string queryEncoded = Uri.EscapeDataString(query);

                string url = "Search.aspx";

                if (searchType == 1) {
                    url += "?type=1";
                } else if (searchType == 2) {
                    url += "?type=2";
                }

                url += "&search=" + queryEncoded;

                Response.Redirect(url);
            }
        }

        //This checks if there are any results from a search. This way we can check first, and then not display the results gridview unless we actually have data to show.
        protected bool foundSearchResults(int searchType) {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            string type = "title";
            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [Threads] WHERE ([" + type + "] LIKE @content)", con);

            //checking searchtype
            if (searchType == 1) {
                type = "title";
            } else {
                type = "content";
            }
            cmd.Parameters.AddWithValue("@content", "%" + lblSearch.Text + "%");

            int postExists = (int)cmd.ExecuteScalar();
            con.Close();

            if (postExists > 0) {
                return true;
            } else {
                return false;
            }
        }

        //when you select a post in the Title-search-result gridview this takes you to said post
        protected void grdTitle_SelectedIndexChanged(object sender, EventArgs e) {
            GridViewRow row = grdTitle.SelectedRow;
            String url = "Post.aspx" + "?id=" + row.Cells[0].Text;
            Response.Redirect(url);
        }

        //when you select a post in the Content-search-result gridview this takes you to said post
        protected void grdContent_SelectedIndexChanged(object sender, EventArgs e) {
            GridViewRow row = grdContent.SelectedRow;
            String url = "Post.aspx" + "?id=" + row.Cells[0].Text;
            Response.Redirect(url);
        }

        //updates the searchType when you change the radiobuttons below the search box
        protected void radioButton_Changed(object sender, EventArgs e) {
            RadioButton button = sender as RadioButton;
            if (button.Checked) {
                if (button == radiobtnTitle) {
                    //Title
                    searchType = 1;
                } else if (button == radiobtnContent) {
                    //Content
                    searchType = 2;
                }
            }
        }
    }
}