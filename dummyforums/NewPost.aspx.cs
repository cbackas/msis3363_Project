﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace dummyforums {
    public partial class NewPost : System.Web.UI.Page {
        string connectionString = "Data Source = (LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\dummyforum.mdf;Integrated Security = True; Connect Timeout = 30";

        protected void Page_Load(object sender, EventArgs e) {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }

    
        protected void btnSubmit_Click(object sender, EventArgs e) {
            if (IsValid) {
                String poster = txtNickName.Text;
                String title = txtTitle.Text;
                String content = txtContent.Text;
                DateTime datetime = System.DateTime.Now;

                InsertComment(poster, title, content, datetime);

                Response.Redirect("Default.aspx");
            }
        }

        protected void InsertComment(string poster, string title, string content, DateTime datetime) {
            string query = "INSERT INTO [Threads] (poster, title, content, datetime) VALUES (@poster, @title, @content, @datetime)";

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@poster", poster);
            cmd.Parameters.AddWithValue("@title", title);
            cmd.Parameters.AddWithValue("@content", content);
            cmd.Parameters.AddWithValue("@datetime", datetime);

            cmd.ExecuteNonQuery();
            con.Close();
        }
    }
}