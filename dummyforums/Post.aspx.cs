﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;

namespace dummyforums {
    public partial class Post : System.Web.UI.Page {
        string connectionString = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\dummyforum.mdf;Integrated Security=True;Connect Timeout=30";

        //When the pages loads it trys to read a post ID from the URL. If it finds an ID it checks if that post actually exists, if it does it shows you that post and if it doesn't it redirects you to a 404 page.
        protected void Page_Load(object sender, EventArgs e) {
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

            string postid = Request.QueryString["id"];
            bool isNumber = System.Text.RegularExpressions.Regex.IsMatch(postid, @"^\d+$");
            if (isNumber && postExists(Convert.ToInt32(postid)))
            {
                lblPostID.Text = postid;
            } else {
                Response.Redirect("404.aspx");
            }
        }

        //Button to submit a new comment. Button gathers the info from the page and sends it to InsertComment() to do the SQL insert
        protected void btnSubmit_Click(object sender, EventArgs e) {
            if (IsValid) {
                string poster = txtNickName.Text;
                string content = txtContent.Text;
                DateTime datetime = System.DateTime.Now;
                int postID = Convert.ToInt32(lblPostID.Text);

                InsertComment(poster, content, datetime, postID);

                Response.Redirect(Request.RawUrl);
            }
        }

        //inserts a new comment with the threadID of the page you're looking at
        protected void InsertComment(string poster, string content, DateTime datetime, int postID) {
            string query = "INSERT INTO [Comments] (poster, content, datetime, threadID) VALUES (@poster, @content, @datetime, @threadID)";

            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand(query, con);
            cmd.Parameters.AddWithValue("@poster", poster);
            cmd.Parameters.AddWithValue("@content", content);
            cmd.Parameters.AddWithValue("@datetime", datetime);
            cmd.Parameters.AddWithValue("@threadID", postID);

            cmd.ExecuteNonQuery();
            con.Close();
        }

        //checks if a post exists so we can decide to show data or redirect
        protected bool postExists(int postID) {
            SqlConnection con = new SqlConnection(connectionString);
            con.Open();

            SqlCommand cmd = new SqlCommand("SELECT COUNT(*) FROM [Threads] WHERE ([Id] = @Id)", con);
            cmd.Parameters.AddWithValue("@Id", postID);

            int postExists = (int)cmd.ExecuteScalar();
            con.Close();

            if (postExists > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
}