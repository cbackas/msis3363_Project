﻿<%@ Page Title="404: Page Not Found" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="dummyforums._404" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="refresh" content="5;url=/404-PAGE" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainPlaceHolder" runat="server">
        <nav>
        <ul class="nav nav-tabs">
            <li><a href="Default.aspx">Home</a></li>
            <li><a href="NewPost.aspx">New Post</a></li>
            <li><a href="Stats.aspx">Stats</a></li>
            <li><a href="Search.aspx">Search</a></li>
        </ul>
    </nav>
<h1>404</h1>
</asp:Content>
