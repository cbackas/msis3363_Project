﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="dummyforums.Threads" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <nav>
        <ul class="nav nav-tabs">
            <li class="active"><a href="Default.aspx">Home</a></li>
            <li><a href="NewPost.aspx">New Post</a></li>
            <li><a href="Stats.aspx">Stats</a></li>
            <li><a href="Search.aspx">Search</a></li>
        </ul>
    </nav>
    <br />
    <br />

    <asp:GridView ID="grdThreads" runat="server"
        AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False"
        CssClass="table table-bordered table-striped table-condensed"
        DataKeyNames="Id" DataSourceID="ThreadsSQL"
        OnPreRender="grdThreads_PreRender"
        OnSelectedIndexChanged="grdThreads_SelectedIndexChanged">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" HeaderStyle-CssClass="hidden" SortExpression="Id">
                <ItemStyle CssClass="hidden" />
            </asp:BoundField>
            <asp:BoundField DataField="poster" HeaderText="User" SortExpression="poster">
                <ItemStyle CssClass="col-xs-1" />
            </asp:BoundField>
            <asp:ButtonField DataTextField="Title" HeaderText="Title" SortExpression="title" CommandName="Select">
                <ItemStyle CssClass="col-xs-8" />
            </asp:ButtonField>
            <asp:BoundField DataField="datetime" DataFormatString="{0:d}" HeaderText="Date" SortExpression="datetime">
                <ItemStyle CssClass="col-xs-2" />
            </asp:BoundField>
        </Columns>
        <HeaderStyle CssClass="card-header" />
        <PagerStyle CssClass="pagination-ys" />
    </asp:GridView>
    <asp:SqlDataSource ID="ThreadsSQL" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Threads] ORDER BY [datetime] DESC, [Id]" OldValuesParameterFormatString="original_{0}"></asp:SqlDataSource>
</asp:Content>
