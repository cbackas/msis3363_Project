﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace dummyforums
{
    public partial class Threads : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e) {
        }

        //gridview pre-render
        protected void grdThreads_PreRender(object sender, EventArgs e) {
            grdThreads.HeaderRow.TableSection = TableRowSection.TableHeader;
        }

        //when you select a post in the gridview this takes you to said post
        protected void grdThreads_SelectedIndexChanged(object sender, EventArgs e) {
            GridViewRow row = grdThreads.SelectedRow;
            String url = "Post.aspx" + "?id=" + row.Cells[0].Text;
            Response.Redirect(url);
        }
    }
}