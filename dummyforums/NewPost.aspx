﻿<%@ Page Title="New Post" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="NewPost.aspx.cs" Inherits="dummyforums.NewPost" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <nav>
        <ul class="nav nav-tabs">
            <li><a href="Default.aspx">Home</a></li>
            <li class="active"><a href="NewPost.aspx">New Post</a></li>
            <li><a href="Stats.aspx">Stats</a></li>
            <li><a href="Search.aspx">Search</a></li>
        </ul>
    </nav>
    <br />
    <br />

    <asp:ValidationSummary ID="ValidationSummary1" runat="server" CssClass="text-danger" HeaderText="* means that the field is required" />

    <h1>New Post:</h1>
    <div class="row padded">
        <div class="col-xs-3">
            <asp:TextBox ID="txtNickName" runat="server"
                Placeholder="Nickname"
                CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-xs-4">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Text="*" Display="Dynamic" ControlToValidate="txtNickName" CssClass="text-danger"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1"
                runat="server" Display="dynamic"
                ControlToValidate="txtNickName"
                ValidationExpression="^([\S\s]{0,30})$"
                ErrorMessage="Nickname maximum: 30 characters"
                CssClass="text-danger">
            </asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="row padded">
        <div class="col-xs-8">
            <asp:TextBox ID="txtTitle" runat="server"
                Placeholder="Title"
                CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-xs-4">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Text="*" Display="Dynamic" ControlToValidate="txtTitle" CssClass="text-danger"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2"
                runat="server" Display="dynamic"
                ControlToValidate="txtTitle"
                ValidationExpression="^([\S\s]{0,200})$"
                ErrorMessage="Title maximum: 200 characters"
                CssClass="text-danger">
            </asp:RegularExpressionValidator>
        </div>
    </div>

    <div class="row padded">
        <div class="form-group">
            <div class="col-xs-8">
                <asp:TextBox ID="txtContent" runat="server"
                    TextMode="MultiLine" Rows="6"
                    Placeholder="New Post..."
                    CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-xs-4">
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" Text="*" Display="Dynamic" ControlToValidate="txtContent" CssClass="text-danger"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3"
                    runat="server" Display="dynamic"
                    ControlToValidate="txtContent"
                    ValidationExpression="^([\S\s]{0,1500})$"
                    ErrorMessage="Content maximum: 1500 characters"
                    CssClass="text-danger">
                </asp:RegularExpressionValidator>
            </div>
        </div>
    </div>

    <div class="row padded">
        <div class="col-xs-1">
            <asp:Button ID="btnsubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" CssClass="btn" CausesValidation="True" />
        </div>
    </div>

</asp:Content>
