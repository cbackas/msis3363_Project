﻿<%@ Page Title="Search" Language="C#" MasterPageFile="~/SiteMaster.Master" AutoEventWireup="true" CodeBehind="Search.aspx.cs" Inherits="dummyforums.Search" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainPlaceHolder" runat="server">
    <nav>
        <ul class="nav nav-tabs">
            <li><a href="Default.aspx">Home</a></li>
            <li><a href="NewPost.aspx">New Post</a></li>
            <li><a href="Stats.aspx">Stats</a></li>
            <li class="active"><a href="Search.aspx">Search</a></li>
        </ul>
    </nav>
    <br />
    <br />

    <asp:Label ID="lblSearch" runat="server" Text="" Visible="False"></asp:Label>

    <div class="row padded">
        <div class="col-xs-3"></div>
        <div class="col-xs-6">
            <asp:TextBox ID="txtSearch" runat="server"
                Placeholder="Search"
                CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-xs-1">
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*" Display="Dynamic" ControlToValidate="txtSearch" CssClass="text-danger"></asp:RequiredFieldValidator>
        </div>
    </div>
    <p />
    <div class="row padded">
        <div class="col-xs-3"></div>
        <div class="col-xs-1">
            <asp:Button ID="btnsubmit" runat="server" Text="Search" OnClick="btnSubmit_Click" CssClass="btn" CausesValidation="True" />
        </div>
        <div class="col-xs-3 radiobtn">
            <asp:RadioButton ID="radiobtnTitle" runat="server" GroupName="radios" Text="Title" OnCheckedChanged="radioButton_Changed" />
            <asp:RadioButton ID="radiobtnContent" runat="server" GroupName="radios" Text="Content" OnCheckedChanged="radioButton_Changed" />
        </div>
    </div>

    <br />

    <div class="row padded">
        <h4 class="text-center"><asp:Label ID="lblmessage" runat="server" Text=""></asp:Label></h4>
    </div>
        
    <br />

    <asp:GridView ID="grdTitle" runat="server"
        AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False"
        CssClass="table table-bordered table-striped table-condensed"
        DataKeyNames="Id" DataSourceID="Threads1SQL"
        OnSelectedIndexChanged="grdTitle_SelectedIndexChanged">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" HeaderStyle-CssClass="hidden" SortExpression="Id">
                <ItemStyle CssClass="hidden" />
            </asp:BoundField>
            <asp:BoundField DataField="poster" HeaderText="User" SortExpression="poster">
                <ItemStyle CssClass="col-xs-1" />
            </asp:BoundField>
            <asp:ButtonField DataTextField="Title" HeaderText="Title" SortExpression="title" CommandName="Select">
                <ItemStyle CssClass="col-xs-8" />
            </asp:ButtonField>
            <asp:BoundField DataField="datetime" DataFormatString="{0:d}" HeaderText="Date" SortExpression="datetime">
                <ItemStyle CssClass="col-xs-2" />
            </asp:BoundField>
        </Columns>
        <HeaderStyle CssClass="card-header" />
    </asp:GridView>

    <asp:SqlDataSource ID="Threads1SQL" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Threads] WHERE ([title] LIKE '%' + @title + '%') ORDER BY datetime DESC, Id DESC" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:ControlParameter ControlID="lblSearch" Name="title" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>

    <asp:GridView ID="grdContent" runat="server"
        AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False"
        CssClass="table table-bordered table-striped table-condensed"
        DataKeyNames="Id" DataSourceID="Threads2SQL"
        OnSelectedIndexChanged="grdContent_SelectedIndexChanged">
        <Columns>
            <asp:BoundField DataField="Id" HeaderText="Id" ReadOnly="True" HeaderStyle-CssClass="hidden" SortExpression="Id">
                <ItemStyle CssClass="hidden" />
            </asp:BoundField>
            <asp:BoundField DataField="poster" HeaderText="User" SortExpression="poster">
                <ItemStyle CssClass="col-xs-1" />
            </asp:BoundField>
            <asp:ButtonField DataTextField="Title" HeaderText="Title" SortExpression="title" CommandName="Select">
                <ItemStyle CssClass="col-xs-8" />
            </asp:ButtonField>
            <asp:BoundField DataField="datetime" DataFormatString="{0:d}" HeaderText="Date" SortExpression="datetime">
                <ItemStyle CssClass="col-xs-2" />
            </asp:BoundField>
        </Columns>
        <HeaderStyle CssClass="card-header" />
    </asp:GridView>

    <asp:SqlDataSource ID="Threads2SQL" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT * FROM [Threads] WHERE ([content] LIKE '%' + @title + '%') ORDER BY datetime DESC, Id DESC" OldValuesParameterFormatString="original_{0}">
        <SelectParameters>
            <asp:ControlParameter ControlID="lblSearch" Name="title" PropertyName="Text" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>
